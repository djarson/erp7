from django.contrib import admin
from .modelos import * 
from .formularios import RegModelForm
# Register your models here.



class PersonalAdmin(admin.ModelAdmin):
	
	form = RegModelForm
	search_fields=('dni','nombres')
	list_filter=['situacion']
	list_display = ('dni', 'nombres')





admin.site.register(Personal, PersonalAdmin)