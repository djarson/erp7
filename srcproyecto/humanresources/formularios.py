from django import forms
from .modelos import Personal,Planilla, ModalidadLaboral, Sectores, Mes, TipoPlanilla, Meta

# class RegForm(forms.Form):
# 	"""docstring for ClassName"""
# 	dni= forms.CharField(max_length=8)
# 	nombres=forms.CharField()

class SearchForm(forms.Form):
	query = forms.CharField(max_length=100)


class RegModelForm(forms.ModelForm):
	class Meta:
		model = Personal
		fields = ["dni","nombres"]

		dni = forms.CharField(required=True,widget=forms.TextInput(attrs={'class':'form-control' , 'autocomplete': 'off','pattern':'[0-9]+', 'title':'Enter numbers Only '}))


	def clean_dni(self):
		dni= self.cleaned_data.get("dni")
		if not dni.isdigit():
			raise forms.ValidationError('El nombre no puede contener letras')
		
		return dni

	def clean_nombres(self):
		nombres=self.cleaned_data.get("nombres")
		if not nombres.isalpha():
			raise forms.ValidationError('El nombre no puede contener numeros') 

		return nombres



class PlanillaPersonalForm(forms.Form):
    mod_laboral = forms.ModelChoiceField(ModalidadLaboral.objects)
    entidad = forms.ModelChoiceField(Sectores.objects)
    mes = forms.ModelChoiceField(Mes.objects)
    tipo_planilla = forms.ModelChoiceField(TipoPlanilla.objects)
    sec_func = forms.ModelChoiceField(Meta.objects)
    fuente_financ = forms.CharField()

    def __init__(self, ano_eje='2014', *args, **kwargs):
        super(PlanillaPersonalForm, self).__init__(*args, **kwargs)
        self.fields['sec_func'].queryset = Meta.objects.filter(ano_eje=ano_eje)

class PlanillaForm(forms.ModelForm):
    class Meta:
        model = Planilla
        fields = ['mes', 'tipo_planilla', 'mod_laboral', 'fuente_financ', 'sec_func', 'entidad']






