from django.conf import settings
# from django.utils import timezone
from django.shortcuts import render

from .modelos import Personal,Planilla,PlanillaTrab,Requerimiento,Orden


from django.views.generic import UpdateView, DetailView, CreateView, TemplateView
from django_genericfilters.views import FilteredListView

from .formularios import RegModelForm,SearchForm,PlanillaPersonalForm,PlanillaForm

from . import constants




class DashboardView(TemplateView):
    template_name = 'pegasus/dashboard.html'

class PersonalList(FilteredListView):
    # Normal ListView options
    model = Personal
    paginate_by = 15
    template_name = 'thumanresources/personal_list.html'

    # FilteredListView options
    form_class = RegModelForm

class PersonalAdd(CreateView):
    model = Personal
    template_name = 'thumanresources/personal_add_edit.html'
    # fields = ['dni', 'nombres']
    form_class = RegModelForm
    success_url = '/rh/personal'

class PersonalEdit(UpdateView):
    model = Personal
    template_name = 'thumanresources/personal_add_edit.html'
    fields = ['dni', 'nombres']
    success_url = '/rh/personal'

class PlanillaList(FilteredListView):
    # Normal ListView options
    model = Planilla
    paginate_by = 15
    template_name = 'thumanresources/planilla_list.html'

    # FilteredListView options
    form_class = SearchForm
    search_fields = ['n_expediente', ]

    def get_queryset(self):
        queryset = super(PlanillaList, self).get_queryset()
        if self.request.current_year:
            queryset = queryset.filter(ano_eje=self.request.current_year)
        return queryset.prefetch_related('mod_laboral', 'tipo_planilla', 'entidad', )


  
class PlanillaAddView(FilteredListView):
    model = Personal
    paginate_by = 100
    form_class = PlanillaPersonalForm
    template_name = 'thumanresources/planilla_add.html'

    # filter_fields = ['mod_laboral', ]

    def form_valid(self, form):
        """Return the queryset when form has been submitted."""
        queryset = super(PlanillaAddView, self).form_valid(form)

        mod_laboral = form.cleaned_data['mod_laboral']
        queryset = queryset.filter(mod_laboral=mod_laboral)

        entidad = form.cleaned_data['entidad']
        queryset = queryset.filter(entidad=entidad)

        return queryset.filter(situacion=constants.SITUACION_ALTA)

    def form_invalid(self, form):
        return Personal.objects.none()


class PlanillaModalAddView(CreateView):
    form = Planilla
    form_class = PlanillaForm
    template_name = 'thumanresources/modals/planilla_add.html'


class PlanillaDetail(DetailView):
    model = Planilla
    template_name = 'thumanresources/planilla_detail.html'


class PlanillaTrabDetail(DetailView):
    model = PlanillaTrab
    template_name = 'thumanresources/planilla_trab_detail.html'

class RequerimientoList(FilteredListView):
    # Normal ListView options
    model = Requerimiento
    paginate_by = 20
    template_name = 'thumanresources/requerimiento_list.html'

    # FilteredListView options
    form_class = SearchForm
    search_fields = ['ano_eje']


class RequerimientoDetailView(DetailView):
    model = Requerimiento
    template_name = 'thumanresources/requerimiento_detail.html'

    def get_object(self, queryset=None):
        year = self.kwargs.get('year', None)
        number = self.kwargs.get('number', None)
        category = self.kwargs.get('category', None) or 'B'

        try:
            obj = Requerimiento.objects.get(ano_eje=year, n_requer=number)

        except Requerimiento.MultipleObjectsReturned:
            obj = Requerimiento.objects.get(ano_eje=year, n_requer=number, clasifica=category)

        except Requerimiento.DoesNotExist:
            raise Http404("No existe el requerimiento")

        return obj



class OrdenList(FilteredListView):
    # Normal ListView options
    model = Orden
    paginate_by = 20
    template_name = 'thumanresources/orden_list.html'

    # FilteredListView options
    form_class = SearchForm
    search_fields = ['ano_eje']

# def inicio(request):
    	
#     	form=RegModelForm(request.POST or None)

#     	context={
    	    	
#     	        "el_form":form,
#     	        # "titulo":userr,
#     	        }

#     	if form.is_valid():
#     		instance=form.save(commit=False)
#     		# dni=form.cleaned_data.get("dni")
#     		# nombres=form.cleaned_data.get("nombres")

    		

#     		instance.save()

#     		context={
#     			"titulo":"gracias %s" %(nombres)
#     		}

#     		print (instance)
#     	return render(request,"inicio.html",context)















